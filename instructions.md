




































































 





 




























































	




 
	





































































































 





 






























































	




 
	
































---
author: Dr. John Noll
module_leader: Dr. John Noll
institute: University of Hertfordshire
course: 7COM1085
term: spring-21
date: \today
tables: true
header-includes: |
  \usepackage{smartdiagram}
  \usesmartdiagramlibrary{additions}
  
...
---
title: Research Questions lab
subtitle: 7COM1085 -- Research Methods
module: 7COM1085
...

---
author: Dr. John Noll
date: Tuesday, 23 Mar, 2021
documentclass: hitec
fontfamily: opensans
fontsize: 12
...
# Overview

This practical focuses on the Research Questions component of the
coursework.  You will submit your research questions using Git, so it
is important that you undestand how to use Git to commit and upload
files to Bitbucket from the command line.  Using the Bitbucket web
interface is not acceptable and will result in reduced marks for the
"Research Infrastructure" component  of the coursework.

# Instructions

#. Read Section 5.3 "The Research Question(s)" of Kitchenham &
 Charters' "Guidelines for performing systematic literature
                  reviews in software engineering".

#. Read the "Research questions" section of the  coursework
 specification, to be sure you understand the requirements for this
 part of the coursework.

#. Discuss potential research topics among yourselves.

#. Decide on a topic, then formulate a general research question.

#. Refine your general research question into one (1)
 specific research question that can be answered by a _systematic
 literature review_ (not an experiment or statistical analysis!).

#. If you haven't already done so, clone your group's Bitbucket repository.

#. Copy `research_question.txt` from this workspace to your group workspace.

#. Change to your group workspace, and open `research_question.txt`
 (using Notepad++ or your favorite text editor, but NOT windows Notepad!).

#. Replace "NN" with your group number.

#. Write the names (but **NOT** student IDs) of each group member
 after `Members: `.

#. Write the general topic area from which you derived your research question.

#. Write your research question after `RQ: `.  Be sure it is an actual _question_!

#. Identify and document the:

    * context,
    * population,
    * intervention,
    * comparison,
    * outcomes.

    Refer to the Kitchenham & Charters guidelines and the lecture notes for an explanation of these terms.

#. Proofread, then commit your *research_questions.txt*.

#. Push your workspace to Bitbucket by the deadline (23:59 2021-03-26).
 

# Example

The following example was proposed during one the practical sessions:

RQ: “Does a higher proportion of women in the population result in higher literacy among women in the developing world?”

Following are the components associated with this question.

* Context: this is the setting or environment in which the question should be interpreted.

    For the above question, the context is _education in the developing world._

* Population: this can be tricky. The population is the group from
  which the sub-groups that will be compared are selected (see the
  Research Design lecture notes for an explanation of sub-groups).

    For questions that involve humans, it’s easier: for our question above, it’s female students.

    But some groups will ask about technology, such as what algorithms produce the best text recognition? In this case, the population is the text that’s being analyzed (such as commit log entries or discussion forum posts).

* Intervention: this is the most difficult.  For obvious cases like
clinical trials, this is the treatment (vaccine, therapy, drug) that
is being tested.  For computer science, this is the algorithm,
technique, tool, etc. that you think might produce a better result.

    My suggestion is to start with the comparison, then ask what will cause there to be a difference between the sub-groups.

    For our question about female students, the “intervention” is the
proportion of women vs men in the sub-group.  So it's not an
intervention in the sense of something we instigate; rather, it's a
distinguishing factor between different sub-groups.

    For the text recognition question, the interventions are different recognition algorithms, which are all applied to the same sub-group in a “cohort” style research design.

* Comparison: this identifies the sub-groups (or “cohort” if there is
  only one).

    For the education question, the comparison is between or among students
in different countries or states.

    For the text recognition question, the comparison is between the
results of applying different algorithms or approaches to the _same group_.

* Outcome: this is the difference the intervention is thought to produce.

    For our education example, this is female literacy. For the text recognition question, the outcome is accuracy: precision, recall, F-statistic, etc.
